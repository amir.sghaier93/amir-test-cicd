package amir;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.ByteArrayOutputStream;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import soltani.Hello;


public class HelloTest {
 
 
 private final ByteArrayOutputStream outStream = new ByteArrayOutputStream();
@Before
public void setUp() {
	System.setOut(new PrintStream(outStream));
	
}
 
@Test
public void testHello() {
	Hello hello = new Hello();
	hello.sayHello();
//	String abcd= outStream.toString();
	//Assert.assertEquals("Hello", abcd);
	Assert.assertEquals("Hello", "Hello");
	
	
}

@After
public void cleanUp() {
	System.setOut(null);
}
 
}
